<?php

namespace Uncgits\EmmaApiLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class EmmaApi extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'EmmaApi'; // the IoC binding.
    }
}
