<?php

namespace Uncgits\EmmaApiLaravel;

use Uncgits\EmmaApi\EmmaApiEndpoint;
use Uncgits\EmmaApi\EmmaApi as BaseApi;
use Uncgits\EmmaApiLaravel\Events\ApiRequestStarted;
use Uncgits\EmmaApi\Clients\EmmaApiClientInterface;
use Uncgits\EmmaApiLaravel\Events\ApiRequestFinished;

class EmmaApi extends BaseApi
{
    protected $cacheBypassed = false;

    public function __construct()
    {
        // set default adapter if specified
        $defaultAdapter = config('emma-api.defaults.adapter');
        if (!empty($defaultAdapter)) {
            $this->setAdapter($defaultAdapter);

            // if we have default adapter, set default config if specified
            $defaultConfigKey = config('emma-api.defaults.config');
            if (!empty($defaultConfigKey)) {
                $this->setConfig(config('emma-api.configs.' . $defaultConfigKey . '.class'));
            }
        }
    }

    public function execute(EmmaApiClientInterface $client, EmmaApiEndpoint $endpoint, $method, $arguments)
    {
        event(new ApiRequestStarted($client, $endpoint, $method, $arguments));

        if (!$this->isCacheable($client, $endpoint, $method, $arguments)) {
            $result = new EmmaApiResult($this->adapter->usingConfig($this->config)->transaction($endpoint));
            $result->setSource('api');

            event(new ApiRequestFinished($client, $endpoint, $method, $arguments, $result));
            return $result;
        }

        // figure out cache key
        $cacheKey = 'emma-api-' . hash('sha3-512', serialize($client) . serialize($endpoint) . $method . json_encode($arguments));

        // check for the cached result first
        if (!is_null($cached = \Cache::get($cacheKey))) {
            event(new ApiRequestFinished($client, $endpoint, $method, $arguments, unserialize($cached)));
            return unserialize($cached)
                ->setSource('cache')
                ->setCacheKey($cacheKey);
        }

        // wasn't cached, so execute the call
        $result = new EmmaApiResult($this->adapter->usingConfig($this->config)->transaction($endpoint));

        if ($result->getStatus() === 'success') {
            // it should be cached if it was successful
            \Cache::put($cacheKey, serialize($result), now()->addMinutes(config('emma-api.cache_minutes', 10)));
        }

        event(new ApiRequestFinished($client, $endpoint, $method, $arguments, $result));
        return $result->setCacheKey($cacheKey);
    }

    public function withoutCache()
    {
        $this->cacheBypassed = true;
        return $this;
    }

    // caching helpers

    private function isCacheable(EmmaApiClientInterface $client, EmmaApiEndpoint $endpoint, $method, $arguments)
    {
        // reset temp cache bypass
        $bypass = $this->cacheBypassed;
        $this->cacheBypassed = false;

        if ($bypass) {
            return false;
        }

        if (!config('emma-api.cache_active')) {
            return false;
        }

        if (strtolower($endpoint->getMethod()) !== 'get') {
            return false;
        }

        $cachedCallsForClient = config('emma-api.cacheable_calls.' . get_class($client));
        if (is_null($cachedCallsForClient)) {
            return false;
        }

        if ($cachedCallsForClient !== ['*'] && !in_array($method, $cachedCallsForClient)) {
            return false;
        }

        return true;
    }
}
