<?php

return [

    // configurations

    'defaults' => [
        'config'  => 'default-account', // config key where 'class' is set
        'adapter' => \Uncgits\EmmaApi\Adapters\Guzzle::class, // adapter class
    ],

    'configs' => [
        'default-account' => [
            'class'          => \App\EmmaApiConfigs\DefaultAccount::class,
            'public_key'     => env('EMMA_API_DEFAULT_PUBLIC_KEY'),
            'private_key'    => env('EMMA_API_DEFAULT_PRIVATE_KEY'),
            'account_id'     => env('EMMA_API_DEFAULT_ACCOUNT_ID'),
            'report_user_id' => env('EMMA_API_DEFAULT_REPORT_USER_ID'),
            'proxy'          => [
                'use'  => env('EMMA_API_DEFAULT_USE_HTTP_PROXY', 'false') == 'true',
                'host' => env('EMMA_API_DEFAULT_HTTP_PROXY_HOST'),
                'port' => env('EMMA_API_DEFAULT_HTTP_PROXY_PORT'),
            ],
        ],
        'other-account' => [
            'class'          => \App\EmmaApiConfigs\OtherAccount::class,
            'public_key'     => env('EMMA_API_OTHER_PUBLIC_KEY'),
            'private_key'    => env('EMMA_API_OTHER_PRIVATE_KEY'),
            'account_id'     => env('EMMA_API_OTHER_ACCOUNT_ID'),
            'report_user_id' => env('EMMA_API_OTHER_REPORT_USER_ID'),
            'proxy'          => [
                'use'  => env('EMMA_API_OTHER_USE_PROXY', 'false') == 'true',
                'host' => env('EMMA_API_OTHER_PROXY_HOST'),
                'port' => env('EMMA_API_OTHER_PROXY_PORT'),
            ],
        ],
        'third-account' => [
            'class'          => \App\EmmaApiConfigs\ThirdAccount::class,
            'public_key'     => env('EMMA_API_THIRD_PUBLIC_KEY'),
            'private_key'    => env('EMMA_API_THIRD_PRIVATE_KEY'),
            'account_id'     => env('EMMA_API_THIRD_ACCOUNT_ID'),
            'report_user_id' => env('EMMA_API_THIRD_REPORT_USER_ID'),
            'proxy'          => [
                'use'  => env('EMMA_API_THIRD_USE_PROXY', 'false') == 'true',
                'host' => env('EMMA_API_THIRD_PROXY_HOST'),
                'port' => env('EMMA_API_THIRD_PROXY_PORT'),
            ],
        ],
    ],

    // caching

    'cache_active'    => env('EMMA_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes'   => env('EMMA_API_CACHE_MINUTES', 10),

    // cache these specific GET requests by client class. use * to cache all
    'cacheable_calls' => [
        // 'Uncgits\EmmaApi\Clients\Members' => [
        //     'listMembers',
        //     'getMember'
        // ],
        // 'Uncgits\EmmaApi\Clients\Groups' => ['*']
    ],

];
