# Emma API Library - Laravel Wrapper

This package is a Laravel wrapper for the UNCG Emma API PHP Library package, so that the Emma API PHP Library can be used in Laravel apps.

---

# Installation

1. `composer require uncgits/emma-api-wrapper-laravel`
2. *IF running Laravel 5.4 or below*: Add `Uncgits\EmmaApiLaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\EmmaApiLaravel\ServiceProvider'` - to publish the `emma-api.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/emma-api.php`

## Dependencies

This package has a dependency on `uncgits/emma-api-php-library`

# Quick Start

## Config and `.env` setup

You'll want to fill in details on your accounts(s) in the published `emma-api.php` file. Most often you likely have only one set of credentials to use, but if you are managing multiple Emma environments from the same application, the flexibility is here for you to put in multiple sets of keys/secrets. Take a look at the default placeholders in the config file and modify them to whatever convention suits you. You will also want to modify the references to the `.env` variables to something that makes sense.

In general, you should consider making **one config file per set of public/private keys you will be using in Emma**; your public/private key won't generally change across Accounts, and there are other ways of specifying which Account you are affecting using this library.

> A deeper explanation: the default `emma-api.php` config file includes a spot for `ACCOUNT_ID`, but it's your choice what to use in the `__construct()` method of your config class. As mentioned in the PHP Library README, you are free to use the fluent `->account()` helper when making your calls, or to set it manually before making a batch of API calls on one Account - you can opt to leave out the `setAccountId()` directive entirely from your `__construct()` method, but the idea is that maybe you want to at least set a default Account ID...

## Set up Config class(es)

In accordance with the documentation on the `uncgits/emma-api-php-library` package, set up a Config class for each set of Emma credentials you wish to use with this package. A recommended namespacing would be `App\EmmaApiConfigs`.

## `.env` variables

At minimum, you should add the account and public/private information for the environment(s) you plan to use with this library.

> Note: do not use protocols (http:// or https://) in your host information

```
EMMA_API_DEFAULT_ACCOUNT_ACCOUNT_ID=
EMMA_API_DEFAULT_ACCOUNT_PUBLIC_KEY=
EMMA_API_DEFAULT_ACCOUNT_PRIVATE_KEY=
```

> These keys are just a suggestion - use whatever you want as you customize your app's `emma-api.php` config file and `.env` file

## Set default Client and Adapter

In the `emma-api.php` config file, fill in both of the items under `defaults`. `defaults.config` should be another config key for that same file that points to the entry in `configs` that you want to use. `defaults.adapter` should point to whichever Adapter class you want to use (either `::class` syntax or the full name as a string will be fine there).

Once you finish this step you should be ready to make your Emma API calls!

---

# Usage

Before proceeding here, please ensure you've read and are familiar with the details of the `uncgits/emma-api-php-library` package.

## Use the facade

The recommended usage for this wrapper package involves the `EmmaApi` root-level facade. Note that this requires you to set the Client you're using (e.g. Accounts, Users, etc.) on each call, however:

```php
$result = \EmmaApi::using('members')->listMembers();
```

## Instantiate the `EmmaApi` class itself

If you prefer, you can instantiate the `Uncgits\EmmaApiLaravel\EmmaApi` class and set a Client class on that instance instead, for better permanence:

```php
$api = new Uncgits\EmmaApiLaravel\EmmaApi; // Config and Adapter classes are pulled in based on defaults in the config file
$api->setClient(new Uncgits\EmmaApi\Clients\Members); // this is not discernable via a default setting, so it must be set.

$result = $api->listMembers();
```

## Configuration options

### Logging

If you wish to implement logging or flash messages or anything of the sort, it is recommended to create a listener that hooks into the `ApiRequestFinished` event, and perform your logic for logging / messages / etc. from there.

### Caching

For speed, adherence to rate limits, and overall performance, caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `EMMA_API_CACHING=off` to your `.env` file.

#### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `EMMA_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes instead.

#### Cached clients / methods

Caching is performed only for specified clients/methods, and is only active for `GET` requests (obviously, you would not want to cache requests that are designed to actually modify information in Emma, like `POST` or `PUT` or `DELETE` requests).

To specify which Clients should be cached, utilize the `cacheable_calls` array in the `emma-api.php` config file:

```php
'cacheable_calls' => [
    // use the Client as the key for an array entry
    'Uncgits\EmmaApi\Clients\Members' => [
        // list each method name you wish to cache here. aliases need to be listed separately!
        'listMembers',
        'getMember',
        // note that any alias methods you use need to be listed here as well - check the Client class to be sure.
    ],

    // to cache all GET transactions made with a client, just use an asterisk instead
    'Uncgits\EmmaApi\Clients\Groups' => ['*']
],
```

#### Determining whether the cache was used

The `EmmaApi` class in this package will return a `EmmaApiResult` class that is an extension of the original, adding two properties: `$source` and `$cacheKey`. You can access either of these on the object using standard getters `getSource()` and `getCacheKey()`.

#### Bypassing the cache

On an individual API call, you can chain `withoutCache()` to bypass the cache on that individual transaction, regardless of what the overall settings are in the application.

## Events

When an API transaction is initiated, the `ApiRequestStarted` event will be dispatched. Similarly, as the result is returned, the `ApiRequestFinished` event will be dispatched. Nothing is set by default to listen to these events, but you can hook into them as needed in your application.

As recommended above, listeners would be a great place to implement logging or other recording of API requests / results in your application.

## Validation Rules

One validation rule is provided by default as an example of some of the things you could add to your forms. Feel free to extend this and/or write your own; pull requests for additional rules would be welcome!

---

# Contributing

Please feel free to submit a pull request to this package to help improve it or fix issues.

# License

See the [LICENSE](LICENSE.md) file for license rights and limitations (BSD).

# Questions? Concerns?

Please use the Issue Tracker on this repository for reporting bugs. For security-related concerns, please contact its-laravel-devs-l@uncg.edu directly instead of using the Issue Tracker.

---

# Version History

## 1.0.1

- Adds BSD info to `composer.json`

## 1.0

- Official open source licensing

## 0.4.1

- change to PSR-4 declaration since we were already following it

## 0.4

- Require `^0.4` on API library package

## 0.3

- Require `^0.3` on API library package

## 0.2

- Clarifications on how to use accounts (e.g. not encouraging a separate Config class unless private/public keys are different)
- encouraging use of fluent `account()` setter if using the Facade.
- require `^0.2` on API library package

## 0.1

- Initial release.
